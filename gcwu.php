<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="<?php echo $locale->gettext('TEXT_LANG'); ?>" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo $locale->gettext('TEXT_LANG'); ?>" dir="ltr">
    <!--<![endif]-->
    <head>
        <?php $this->insert('layouts::head', ['title' => $title]); ?>
        <?=$this->section('head'); ?>
    </head>
    <body vocab="http://schema.org/" typeof="WebPage">
        <ul id="wb-tphp">
            <li class="wb-slc">
                <a class="wb-sl" href="#wb-cont"><?php echo $locale->gettext('TEXT_SKIP_MAIN'); ?></a>
            </li>
            <li class="wb-slc visible-sm visible-md visible-lg">
                <a class="wb-sl" href="#wb-info"><?php echo $locale->gettext('TEXT_SKIP_ABOUT'); ?></a>
            </li>
        </ul>

        <?php $this->insert('layouts::header', ['urlAltLang'=>$urlAltLang]); ?>

        <?php if ($columns == 1) : ?>
        <main role="main" property="mainContentOfPage" class="container">
            <?= $this->section('content') ?>
        </main>
        <?php else : ?>
        <div class="container">
            <div class="row">
                <main role="main" property="mainContentOfPage" class="col-md-9 col-md-push-3">
                    <?= $this->section('content') ?>
                </main>

                <?=$this->section('left', $this->fetch('layouts::left')); ?>
            </div>
        </div>
        <?php endif; ?>


        <?php $this->insert('layouts::footer'); ?>
        <!--[if gte IE 9 | !IE ]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
        <script src="<?=$baseUri; ?>/app/gcwu/wet-boew/js/wet-boew.min.js"></script>

        <!--<![endif]-->
        <!--[if lt IE 9]>
		<script src="<?=$baseUri; ?>/app/gcwu/wet-boew/js/ie8-wet-boew2.min.js"></script>
		<![endif]-->
        <script src="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/js/theme.min.js"></script>

        <?=$this->section('scripts'); ?>

    </body>
</html>
