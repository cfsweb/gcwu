        <header role="banner">
            <div id="wb-bnr">
                <div id="wb-bar">
                    <div class="container">
                        <div class="row">
                            <object id="gcwu-sig" type="image/svg+xml" tabindex="-1" role="img" data="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/assets/sig-<?php echo $locale->gettext('TEXT_LANG'); ?>.svg" aria-label="<?php echo $locale->gettext('TEXT_GOC'); ?>"></object>
                            <ul id="gc-bar" class="list-inline">
                                <li><a href="<?php echo $locale->gettext('URL_CANADA_CA'); ?>" rel="external">Canada.ca</a></li>
                                <li><a href="<?php echo $locale->gettext('URL_SERVICES'); ?>" rel="external"><?php echo $locale->gettext('TEXT_SERVICES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_DEPARTMENTS'); ?>" rel="external"><?php echo $locale->gettext('TEXT_DEPARTMENTS'); ?></a></li>
                                <li id="wb-lng"><h2><?php echo $locale->gettext('TEXT_LANG_SELECTION'); ?></h2>
                                    <ul class="list-inline">
                                        <li><a lang="<?php echo $locale->gettext('TEXT_ALT_LANG'); ?>" href="<?=$this->e($urlAltLang); ?>"><?php echo $locale->gettext('TEXT_ALT_LANG_FULL'); ?></a></li>
                                    </ul>
                                </li>
                            </ul>
                            <section class="wb-mb-links col-xs-12 visible-sm visible-xs" id="wb-glb-mn">
                                <h2><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></h2>
                                <ul class="pnl-btn list-inline text-right">
                                    <li><a href="#mb-pnl" title="<?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?>" aria-controls="mb-pnl" class="overlay-lnk btn btn-sm btn-default" role="button"><span class="glyphicon glyphicon-search"><span class="glyphicon glyphicon-th-list"><span class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></span></span></span></a></li>
                                </ul>
                                <div id="mb-pnl"></div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div id="wb-sttl" class="col-md-5">
                            <a href="<?=$locale->gettext('URL_NRCAN'); ?>">
                            <span><?php echo $locale->gettext('TEXT_NRCAN_FULL'); ?></span>
                            </a>
                        </div>
                        <object id="wmms" type="image/svg+xml" tabindex="-1" role="img" data="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/assets/wmms.svg" aria-label="<?php echo $locale->gettext('TEXT_WORDMARK'); ?>"></object>
                        <section id="wb-srch" class="visible-md visible-lg">
                            <h2>Search</h2>
                            <form action="<?=$locale->gettext('URL_NRCAN_SEARCH'); ?>" method="get" role="search" class="form-inline">
                                <input type="hidden" value="s" name="st">
                                <input type="hidden" value="10" name="num">
                                <input type="hidden" value="x" name="s5bm3ts21rch">
                                <input type="hidden" value="0" name="st1rt">
                                <input type="hidden" value="<?=$locale->gettext('TEXT_DCLANG'); ?>" name="langs">
                                <input type="hidden" value="rncannrcan" name="cdn">
                                <div class="form-group">
                                    <label for="wb-srch-q"><?php echo $locale->gettext('TEXT_SEARCH_WEBSITE'); ?></label>
                                    <input id="wb-srch-q" class="form-control" name="q" type="search" value="" size="27" maxlength="150">
                                </div>
                                <button type="submit" id="wb-srch-sub" class="btn btn-default"><?php echo $locale->gettext('TEXT_SEARCH'); ?></button>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
            <nav role="navigation" id="wb-sm" data-ajax-replace="//www.nrcan.gc.ca/sites/www.nrcan.gc.ca/files/menus/sitemenu-<?=$lang; ?>.html" data-trgt="mb-pnl" class="wb-menu visible-md visible-lg" typeof="SiteNavigationElement">
                <div class="container nvbar">
                    <h2><?php echo $locale->gettext('TEXT_TOPICS_MENU'); ?></h2>
                    <div class="row">
                        <ul class="list-inline menu">
                            <li><a href="<?php echo $locale->gettext('URL_OUR_NAT_RESOURCES'); ?>"><?php echo $locale->gettext('TEXT_OUR_NAT_RESOURCES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_CLIMATE_CHANGE'); ?>"><?php echo $locale->gettext('TEXT_CLIMATE_CHANGE'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_ENERGY_EFFICIENCY'); ?>"><?php echo $locale->gettext('TEXT_ENERGY_EFFICIENCY'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_MAPS_TOOLS'); ?>"><?php echo $locale->gettext('TEXT_MAPS_TOOLS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PUBLIC_CONSULT'); ?>"><?php echo $locale->gettext('TEXT_PUBLIC_CONSULT'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_SCIENCE_DATA'); ?>"><?php echo $locale->gettext('TEXT_SCIENCE_DATA'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav role="navigation" id="wb-bc" property="breadcrumb">
                <h2><?php echo $locale->gettext('TEXT_YOU_ARE_HERE'); ?></h2>
                <div class="container">
                    <div class="row">
                        <?php echo $this->breadcrumb()->display(); ?>
                    </div>
                </div>
            </nav>
        </header>