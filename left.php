                <nav role="navigation" id="wb-sec" typeof="SiteNavigationElement" class="col-md-3 col-md-pull-9 visible-md visible-lg">
                    <h2><?=$locale->gettext('TEXT_SECTION_MENU'); ?></h2>
                    
                    <ul class="list-group menu list-unstyled">
                        <li>
                            <h3 class="wb-navcurr"><?=$locale->gettext('TEXT_NAV_HEADER'); ?></h3>
                            
                            <ul class="list-group menu list-unstyled">
                                <li><a class="list-group-item" href="#">Heading</a>
                                    <ul class="list-group menu list-unstyled">
                                        <li><a class="list-group-item" href="#>">Link Text</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>