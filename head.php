        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <title><?=$this->e($title); ?></title>
        <meta content="width=device-width,initial-scale=1" name="viewport">
        <!-- Meta data -->
        <meta name="dcterms.title" content="<?=$this->e($title); ?>">
        <meta name="dcterms.creator" content="<?=$locale->gettext('TEXT_NRCAN_FULL'); ?>">
        <meta name="dcterms.issued" title="W3CDTF" content="">
        <meta name="dcterms.modified" title="W3CDTF" content="">
        <meta name="dcterms.language" title="ISO639-2" content="<?php echo $locale->gettext('TEXT_DCLANG'); ?>">
        <!-- Meta data-->
        <!--[if gte IE 9 | !IE ]><!-->
        <link href="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/assets/favicon.ico" rel="icon" type="image/x-icon">
        <link rel="stylesheet" href="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/css/theme.min.css">
        
        <!--<![endif]-->
        <!--[if lt IE 9]>
        <link href="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/assets/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="<?=$baseUri; ?>/app/gcwu/theme-gcwu-fegc/css/ie8-theme.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="<?=$baseUri; ?>/app/gcwu/wet-boew/js/ie8-wet-boew.min.js"></script>
        <![endif]-->
        
        <noscript><link rel="stylesheet" href="<?=$baseUri; ?>/app/gcwu/wet-boew/css/noscript.min.css" /></noscript>
        

    

